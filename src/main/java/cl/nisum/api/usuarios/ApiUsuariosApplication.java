package cl.nisum.api.usuarios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiUsuariosApplication {
	public static final String REST_SERVICE_URI = "http://localhost:8080/ApiUsuarios/";

	public static void main(String[] args) {
		SpringApplication.run(ApiUsuariosApplication.class, args);
	}


}
