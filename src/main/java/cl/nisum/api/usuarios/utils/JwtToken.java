package cl.nisum.api.usuarios.utils;


import java.util.Date;

import cl.nisum.api.usuarios.entity.Usuario;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
public class JwtToken {

    private String secret = "miclavesecreta";

    public Usuario parseToken(String token) {
        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();

            Usuario u = new Usuario();
            u.setName(body.getSubject());

            return u;

        } catch (JwtException | ClassCastException e) {
            return null;
        }
    }

    public String generateToken(Usuario u) {
        Claims claims = Jwts.claims().setSubject(u.getName());

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(new Date(System.currentTimeMillis()))
				//.setExpiration(new Date(System.currentTimeMillis() + 600000))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }
}
