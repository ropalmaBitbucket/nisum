package cl.nisum.api.usuarios.utils;

public class UsuarioError {
	private String mensaje;

	public UsuarioError(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getMensaje() {
		return mensaje;
	}

}
