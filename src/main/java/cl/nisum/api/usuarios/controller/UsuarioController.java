package cl.nisum.api.usuarios.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import cl.nisum.api.usuarios.entity.Usuario;
import cl.nisum.api.usuarios.utils.JwtToken;
import cl.nisum.api.usuarios.utils.UsuarioError;

@RestController
public class UsuarioController {
	@Autowired
	private UsuarioDao usuarioDao;

	private static final Logger logger = LoggerFactory.getLogger(UsuarioController.class);

	@PostMapping(value = "/usuario", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> createUser(@Valid @RequestBody Usuario usuario) {
		logger.info("Creando usuario : {}", usuario);

		JwtToken jwtToken = new JwtToken();
		final String token = jwtToken.generateToken(usuario);
		UUID uuid = UUID.randomUUID();
		String randomUUIDString = uuid.toString();
		Usuario usuarioCreado = new Usuario();
		usuario.setToken(token);
		usuario.setUuid(randomUUIDString);
		usuario.setIsactive(true);
		try {
			usuarioCreado = usuarioDao.guardar(usuario);

		} catch (DataIntegrityViolationException e) {
			logger.error("Error al crear usuario : {}", e.getLocalizedMessage());
			return new ResponseEntity<>(new UsuarioError("Un usuario con correo " + usuario.getEmail() + " ya existe."),
					HttpStatus.CONFLICT);
		}

		usuarioCreado.setLastLogin(usuarioCreado.getCreated());

		return new ResponseEntity<>(usuarioCreado, HttpStatus.CREATED);
	}

}
