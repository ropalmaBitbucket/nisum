package cl.nisum.api.usuarios.controller;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import cl.nisum.api.usuarios.entity.Phone;
import cl.nisum.api.usuarios.entity.Usuario;

@Repository
public class UsuarioDaoImpl implements UsuarioDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional
	public Usuario guardar(Usuario usuario) {
		for (Phone phone : usuario.getPhones()) {
			entityManager.persist(phone);
		}
		entityManager.persist(usuario);

		return usuario;
	}

}
