package cl.nisum.api.usuarios.controller;

import cl.nisum.api.usuarios.entity.Usuario;

public interface UsuarioDao {
	public Usuario guardar(Usuario usuario);
}
