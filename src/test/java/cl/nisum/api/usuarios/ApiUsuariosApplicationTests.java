package cl.nisum.api.usuarios;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

//import com.fasterxml.jackson.databind.ObjectMapper;

import cl.nisum.api.usuarios.controller.UsuarioDao;
import cl.nisum.api.usuarios.entity.Usuario;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT) // for restTemplate
class ApiUsuariosApplicationTests {
	public static final String REST_SERVICE_URI = "http://localhost:8080";


	@Autowired
	private TestRestTemplate restTemplate;

	@MockBean
	private UsuarioDao usuarioDao;

	@Test
	public void guardar_usuario() throws JSONException {

		String usuarionJson = "{\"name\": \"Juan Rodriguez\",\"email\": \"ju324@rodriguez.cl\",\"password\": \"Runter23\","
				+ "\"phones\":[{\"number\": \"22345688\",\"citycode\": 1,\"contrycode\":59},"
				+ "{\"number\": \"55347688\",\"citycode\": \"20\",\"contrycode\":\"58\"}]}";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<>(usuarionJson, headers);

		ResponseEntity<String> response = restTemplate.postForEntity(REST_SERVICE_URI + "/usuario", entity,
				String.class);

		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		verify(usuarioDao, times(0)).guardar(any(Usuario.class));

	}


}
